Où continuer après ce tutoriel?
===============================

Voici quelques ressources qui vous permettront d'en apprendre plus sur Scheme et
son intégration à LilyPond.

- La `documentation de Guile`__ est une référence technique indispensable pour
  les projets ambitieux. Attention, la version actuellement embarquée par
  LilyPond est Guile 1.8.

  __ https://www.gnu.org/software/guile/learn/

- Le `manuel d'extension`__ dans la documentation de LilyPond contient des
  informations sur quelques unes des principales interfaces de programmation.
  Malheureusement incomplet à ce jour, il présente l'avantage d'être traduit en
  français.

  __ https://lilypond.org/extending.fr.html 

- `Extending LilyPond`__ (écrit par moi-même), un guide beaucoup plus complet
  mais pour l'instant seulement en anglais.

  __ https://extending-lilypond.readthedocs.io/en/latest/
