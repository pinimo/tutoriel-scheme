Pour démarrer
=============

Le bac à sable Scheme
---------------------

Le bac à sable (*sandbox* en anglais) est un moyen pratique de faire des tests.
Il s'agit d'une invite interactive, ou Read-Eval-Print-Loop: on saisit du code
Scheme, il est exécuté au fur et à mesure, et le résultat est affiché. On peut
le voir comme une calculatrice.

Pour lancer le bac à sable, il faut ouvrir un terminal. Les utilisateurs de
GNU/Linux trouveront bien. Sous Mac OS X, lancer l'application *Terminal*. Sous
Windows, se placer dans un dossier quelconque dans l'explorateur de fichiers et
sélectionner Windows PowerShell dans le menu Fichier.

Dans le terminal, exécuter la commande::

   lilypond scheme-sandbox

En fonction du type d'installation, il faudra peut-être spécifier le chemin
complet vers l'exécutable ``lilypond`` (s'il n'est pas dans les chemins de
recherche). Sous Mac OS, essayer::

   /Applications/LilyPond.app/Contents/Resources/bin/lilypond scheme-sandbox

Sous Windows, ::

   "C:\Program Files\LilyPond\usr\bin" scheme-sandbox

ou ::

   "C:\Program Files (x86)\LilyPond\usr\bin" scheme-sandbox

*(Je n'ai aucune possibilité de tester les commandes sous Windows. Si cela ne
fonctionne pas, écrivez à la liste.)*

On obtient alors un écran comme celui-ci::

   $ lilypond scheme-sandbox
   GNU LilyPond 2.22.1
   Traitement de « /.../lilypond/usr/share/lilypond/current/ly/scheme-sandbox.ly »
   Analyse...
   guile> 

La ligne ``guile>`` nous indique que Guile est là, prêt à nous servir. En
saisissant un extrait de code, il est évalué. Commençons par le plus simple, un
nombre. ::

   guile> 42
   42

À nouveau, le principe est similaire à une calculette: 42 s'évalue bien à 42.
Écrivons maintenant un calcul (la syntaxe sera expliquée bientôt)::

   guile> (+ 42 5)
   47

Ici, on a effectué l'addition 42 + 5, qui donne bien 47.  Vous pouvez suivre ce
tutoriel en copiant les exemples de code dans le bac à sable et en observant ce
qui se passe avec des variantes de ces programmes.


Insertion de code Scheme dans un fichier LilyPond
-------------------------------------------------

Quasiment partout dans le code d'une partition, on peut passer en Scheme à
l'aide d'un croisillon (caractère #). La lecture s'arrête dès qu'une expression
Scheme complète est terminée. Les exemples suivants seront familiers:

::

   #(set-global-staff-size 18)

::

   \markup \vspace #5

::

   \shape #'((0 . 0) (0.1 . 0.3) (0.1 . 0.5) (0 . 1.0)) Slur 

Scheme est souvent utilisé pour saisir des valeurs, comme le nombre 5 pour
``\markup \vspace``, ou bien la liste des décalages passée à ``\shape``.  Si on
saisit ces valeurs telles quelles, elles se perdent le plus souvent dans la
nature, contrairement au bac à sable qui les affiche. Par exemple, le fichier
LilyPond suivant ne produit ni sortie graphique ni message dans la console. ::

   #'((0 . 0) (0.1 . 0.3) (0.1 . 0.5) (0 . 1.0))

On peut provoquer l'affichage d'une valeur dans le fichier journal à l'aide de
la fonction ``display`` (admettez pour l'instant la syntaxe, qui sera
décortiquée plus tard)::

   #(display '((0 . 0) (0.1 . 0.3) (0.1 . 0.5) (0 . 1.0)))

À l'intérieur d'une expression Scheme, il existe la possibilité pratique
d'insérer une partie en syntaxe LilyPond, entourée par les délimiteurs ``#{`` et
``#}``. Par exemple::

   #(skip-of-length #{ { c'8 d' e' f' e' d' } #})


Commentaires
------------

En LilyPond, les commentaires commencent par un signe ``%`` et courent jusqu'à
la fin d'une ligne. La syntaxe Scheme est légèrement différente, avec le
caractère ``;``. Le principe reste le même: les commentaires permettent
d'expliquer le code source pour le lecteur humain.  Ils sont totalement ignorés
par l'interpréteur. ::

   ; Ceci est un commentaire. Il n'a aucun effet.
   42.0 ; Un autre commentaire, après une ligne de code.

Il existe également une syntaxe de commentaires multilignes, équivalente à
``%{`` et ``%}`` en LilyPond. Les délimiteurs sont ``#!`` et ``!#``. ::

   #!/usr/bin/guile
   Nous voici dans un commentaire multiligne. Les utilisateurs
   de systèmes UNIX (comme GNU/Linux et Mac OS X) auront remarqué
   que la syntaxe de ces commentaires est adaptée aux shebangs.
   Si vous ne savez pas ce que c'est, cela n'a aucune importance.
   !#


Littéraux
---------

Cette partie traite de l'écriture des valeurs courantes, que tout utilisateur de
LilyPond rencontre régulièrement.

Commençons par le commencement: les nombres. Ils s'écrivent en Scheme comme
autre part::

   5
   -10

Les nombres décimaux s'écrivent avec un point (notation anglo-saxonne). ::

   5.3
   -10.42

Quant aux chaînes de caractères, c'est à dire des extraits de texte comme suites
de caractères, elles se saisissent entre guillemets doubles. ::

   "Sonata IV"

Elles peuvent contenir des retours à la ligne. ::

   "J'entends le loup, le renard et la belette,
   J'entends le loup et le renard chanter."

On peut également indiquer les retours à la ligne à l'aide de ``\n``. Les deux
formes sont strictement équivalentes. ::

   guile> "J'entends le loup, le renard et la belette,\nJ'entends le loup et le renard chanter."
   "J'entends le loup, le renard et la belette,
   J'entends le loup et le renard chanter."

Pour écrire un guillemet dans la chaîne, il faut le faire précéder d'un
antislash afin qu'il ne soit pas interprété comme la fin de la chaîne. ::

   "Rondo \"alla turca\""


Pour afficher une chaîne, il suffit d'appeler dessus la fonction ``display``. ::

   guile> (display "Rondo \"alla turca\"\n")
   Rondo "alla turca"

Il faut bien faire la différence entre la manière dont on écrit une chaîne et la
manière dont elle s'affiche. Une chaîne de caractères n'est rien qu'une suite de
caractères. La chaîne ``"\"\n"``, en dépit des apparences, ne contient que deux
caractères, un guillemet et un retour à la ligne. S'il faut mettre ces
antislashs, c'est simplement pour éviter une confusion dans la lecture de la
chaîne.  Cependant, ``display`` nous confirme le contenu de la chaîne::

   guile> (display "\"\n")
   "
   guile> 

Notez que LilyPond accepte pour les nombres et chaînes de caractères une syntaxe
proche de celle de Scheme. Ainsi, les codes suivants sont équivalents::

   \override TextScript.Y-offset = 5.2
   \override TextScript.Y-offset = #5.2

De même, on a l'équivalence entre::

   title = #"Rondo"
   title = "Rondo"
   title = Rondo

(Bien entendu, la troisième syntaxe ne peut fonctionner qu'avec un seul mot, et
il faut des guillemets pour une chaîne de caractères contenant des espaces.)


.. _variables:

Définition de variables
-----------------------

Les utilisateurs de LilyPond savent que leur code sera bien plus simple s'ils
l'organisent à l'aide de variables. ::

   clarinette = \relative {
     ...
   }

   { \clarinette }
   { \transpose c d \clarinette }

L'avantage est double. D'une part, il est possible de se servir de la valeur en
plusieurs endroits distincts sans la recopier. D'autre part, même pour un seul
usage, définir une variable rend le code plus compréhensible et donc plus facile
à modifier plus tard.

En Scheme, les variables se définissent à l'aide de la syntaxe::

   (define nom valeur)

Par exemple, pour affecter à ``x`` la valeur 5, on écrira::

   (define x 5)

Par la suite, il suffit d'écrire le nom de la variable, qui est automatiquement
remplacé par sa valeur (alors que LilyPond a besoin d'un antislash). ::

   (define x 5)
   (display x)

Si le nom d'une variable contient plusieurs mots, la convention est de les
séparer par des tirets. ::

   (define ma-variable 44)

De manière peut-être surprenante, les noms de variables peuvent contenir
quasiment tous les caractères spéciaux imaginables. Seules les parenthèses,
guillemets et quelques autres ne sont pas valides. ::

   (define x+|^ 34)
   (display x+|^)
