Fonctions
=========

L'intérêt des fonctions (ou procédures) est de créer du code modulaire, qui peut
être réutilisé. Cependant, les fonctions servent à bien d'autres tâches encore
en Scheme, qui fait partie de la famille des langages dits fonctionnels, terme
qui s'éclaircira un peu plus tard. La syntaxe la plus courante pour définir une
fonction est une extension du même ``define`` qui servait à définir des
variables::

   (define (nom-de-la-fonction argument1 argument2 ...)
     corps de la fonction...)

Le corps de la fonction est une suite d'expressions. Comme dans un ``begin``,
elles sont évaluées séquentiellement et la valeur de la dernière est renvoyée
par la fonction. Voici un premier exemple::

   (define c 299792458)

   (define (E m)
     (* m c c))

Après avoir défini cette fonction, on peut l'appeler::

   (E 56.6)

Une application des expressions qui peuvent se situer avant la dernière est
d'afficher des messages de déboguage::

   (define (double x)
     (format #t  "Fonction double appelée avec le paramètre ~a\n" x)
     (* 2 x))
