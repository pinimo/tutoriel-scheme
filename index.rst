Apprendre Scheme pour LilyPond
##############################

Ce tutoriel, issu d'une présentation au Café Lily 2021 en visioconférence, vous
permettra d'acquérir les bases du langage Scheme, avec lequel il est possible de
programmer chaque élément d'une partition LilyPond__.

__ https://lilypond.org

Merci à Urs Liska, dont le début de `livre numérique`__ (incomplet à ce jour)
sur Scheme et LilyPond m'a permis d'apprendre Scheme et a inspiré certaines
parties de ce tutoriel.

__ https://scheme-book.ursliska.de/introduction/index.html

Les questions sur Scheme sont les bienvenues sur la `liste francophone`__. Pour
proposer une amélioration à ce document ou signaler une erreur,
`contactez-moi`__ directement.

__ http://lists.gnu.org/mailman/listinfo/lilypond-user-fr
__ mailto:jean@abou-samra.fr

Ce document est placé sous licence `Creative Commons CC0`__.

__ https://creativecommons.org/publicdomain/zero/1.0/

.. toctree::
   pourquoi-scheme
   demarrer
   expressions
   fonctions
   messages
   conditions
   variables-locales
   listes
   quoting
   alists
   recursivite
   continuer

.. typage
