Messages
========

Afficher du texte dans la console s'avère essentiel pour déboguer un programme.
On dispose de plusieurs moyens.

La procédure ``display`` affiche un objet quelconque (chaîne de caractères ou
autres). On peut s'en servir plusieurs fois pour afficher plusieurs objets.
Aucun retour à la ligne n'est automatique, il faut donc soit terminer la chaîne
de caractères que l'on affiche par ``\n`` (si on affiche une chaîne de
caractères), soit ajouter un ``(display "\n")``, que l'on abrège en
``(newline)``. Par exemple, le code::

   (define prénom "Salima")
   (display "Bonjour ")
   (display prénom)
   (display " !\n")

s'écrit encore::

   (define prénom "Salima")
   (display "Bonjour ")
   (display prénom)
   (display " !")
   (newline)

Un moyen plus agréable est d'employer la procédure ``format``. Dans un premier
temps, nous allons mettre son premier argument à ``#t`` (patientez un peu pour
comprendre cet objet). Le deuxième argument est une chaîne de caractères, qui
peut contenir des séquences spéciales. Les arguments suivants doivent être au
même nombre que les séquences spéciales. Ils sont formatés d'une certaine
manière suivant la séquence, et insérés dans la chaîne. Le tout est envoyé sur
la console. Une séquence spéciale usuelle est ``~a``, qui formate les objets
exactement comme ``display``. ::

   guile> (define prénom "Salima")
   guile> (format #t "Bonjour ~a !\n" prénom)
   Bonjour Salima !
   #t


Avec ``#t``, la valeur renvoyée par ``format`` n'est pas importante. En mettant
``#f`` à la place, ``format`` renvoie la chaîne de caractères formatée, sans
l'afficher directement. ::

   guile> (define prénom "Salima")   
   guile> (format #f "Bonjour ~a !\n" prénom)    
   "Bonjour Salima !
   "

Par exemple, LilyPond se sert intensivement de ``format`` avec ``#f`` pour
construire la sortie PostScript. L'extrait suivant provient du fichier
framework-ps.scm__. ::

   (format #f "%%BeginDocument: ~a
   ~a
   %%EndDocument
   "
             file-name
             (cached-file-contents file-name))

__ https://gitlab.com/lilypond/lilypond/-/blob/master/scm/framework-ps.scm

La procédure ``ly:message`` fonctionne comme ``format``. Son seul but est
d'afficher des messages, donc elle ne prend pas le premier argument ``#t`` ou
``#f``. Elle demande une chaîne de caractères, éventuellement suivie d'arguments
correspondant aux séquences spéciales. Son avantage est d'afficher le message
dans le flux d'erreur standard, et non pas dans la sortie standard, ce qui
l'empêche d'être entremêlé avec le reste du journal dans l'interface de
Frescobaldi ou autre (on pourrait remplacer ``#t`` par ``(current-error-port)``
dans ``format`` pour obtenir le même effet). De plus, ``ly:message`` ajoute
automatiquement un retour à la ligne. ::

   (define fichier "emmentaler-20.otf")
   (ly:message "Lecture du fichier ~a..." fichier)

Il existe également la séquence spéciale ``~s``, qui formate les valeurs comme
le ferait le bac à sable. Par exemple, elle met des guillemets autour des
chaînes de caractères. Elle est plus adaptée au déboguage. ::

   (define fichier "emmentaler-20.otf")
   (ly:message "Lecture du fichier ~s..." fichier)
