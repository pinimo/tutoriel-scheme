Pourquoi Scheme?
================

Le langage Scheme est un dérivé du Common Lisp, pensé au départ pour le cursus
d'informatique au MIT. De là vient qu'il vise à la cohérence et la simplicité,
voire au minimalisme.

LilyPond se sert de Scheme comme d'un langage d'extension. Il s'agit d'une
technique courante: le cœur du programme est écrit dans un langage *compilé*, en
l'occurrence C++, et des interfaces sont fournies afin d'interagir avec le
programme à l'aide d'un langage *interprété*.  La compilation signifie que le
programme est traduit en code binaire une fois pour toutes, ce qui a l'avantage
de la rapidité d'exécution.  Cependant, les fonctionnalités sont figées dans le
programme, sans que l'utilisateur ne puisse l'adapter à ses besoins à moins de
passer par un lourd processus de recompilation. Au contraire, avec un
interpréteur, le code est exécuté au fur et à mesure, ce qui apporte la
flexibilité de le modifier facilement.

À l'intérieur de LilyPond, Scheme est littéralement dans chaque recoin.  C'est
ce qui donne à ce logiciel un niveau d'extensibilité rare.  Pour un utilisateur,
les applications sont multiples, de la création de raccourcis de saisie jusqu'à
l'implémentation de notations graphiques propres à un compositeur.

Sans aller jusque là, connaître Scheme est une compétence utile à tous les
utilisateurs quand on considère que la syntaxe Scheme se retrouve à bien des
niveaux dans la saisie d'une partition courante:

::

   title = ##f

::

   \override NoteHead.style = #'cross 

::

   \shape #'((0 . 0) (0.1 . 0.3) (0.1 . 0.5) (0 . 1.0)) Slur

::

   \override Staff.TupletBracket.direction = #DOWN

Il faut savoir qu'il n'existe pas une implémentation du langage Scheme, mais des
dizaines, chacune avec ses spécificités. L'implémentation choisie pour LilyPond
est Guile_, qui est le langage d'extension officiel du projet de système
d'exploitation libre GNU_. Le nom "Guile" est l'acronyme de "GNU Ubiquitous
Intelligent Language for Extensions", soit "Langage GNU Omniprésent Intelligent
pour les Extensions". (L'adjectif "omniprésent" relève plus ou moins du
mensonge; pour "intelligent", on laissera le lecteur juge.)

Il est important de garder à l'esprit que la version de Guile actuellement
embarquée dans les versions officielles de LilyPond est Guile 1.8, et non pas la
version plus récente, Guile 3.0 [#]_.  Le manuel de Guile 1.8 se trouve à
l'adresse:

   https://www.gnu.org/software/guile/docs/docs-1.8/guile-ref/

.. _Guile: https://www.gnu.org/software/guile
.. _GNU: https://www.gnu.org/


--------------------------------------------------------------------------------------

.. [#] Il est possible à l'heure actuelle de compiler LilyPond avec Guile 2.2,
       avec cependant quelques problèmes qui susbistent, et doivent être résolus avant
       la transition finale.
